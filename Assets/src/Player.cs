﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	static bool gyroBool;
	private Gyroscope gyro;
	private Quaternion quatMult;
	private Quaternion quatMap;

	private Text GPSStatusText;
	private Text PositionText;
	private Slider moveMultSlider;
	private bool serviceEnabled = false;
	public float lastLatitude;
	public float lastLongitude;

	private GameObject gpsMarker;
	public MeshProperties mesh;
	public Vector3 Velocity;
	public Vector3 LastPos;
	private KalmanGPS gps;
	private GameObject AccuracySphere;

	private List<Vector3> previousVelocities;
	
	private float new_player_orientation = 0.0f;

	private List<float> previous_orientations = new List<float>();

	public float player_orientation = 0.0f;
	public float snitch_direction;
	private AudioSource directionAudio;
	private int numberOfIterations = 10;
	public List<AudioClip> StoryClips;
	public Dictionary<int,int> StoryClipSequences;
	private GameObject snitch;
	public List<AudioClip> FeedbackFaster;
	public List<AudioClip> FeedbackSlower;

	IEnumerator Start () {
		GPSStatusText = GameObject.Find ("GPSStatusText").GetComponent<Text> ();
		PositionText = GameObject.Find ("PositionText").GetComponent<Text> ();
		gpsMarker = GameObject.Find ("GPS");
		AccuracySphere = GameObject.Find("AccuracySphere");
		directionAudio = GetAudioPlayer(AudioPlayerType.DirectionStatic);
		Velocity = Vector3.zero;
		snitch = GameObject.Find("Snitch");
		previousVelocities = new List<Vector3>();
		
		moveMultSlider = GameObject.Find ("MoveMultiplierSlider").GetComponent<Slider> ();
		mesh = GameObject.Find("MRC").GetComponent<MeshProperties>();
		gps = new KalmanGPS(moveMultSlider.value); //TODO: 3 m/s loopsnelheid
		//var tmp = Tools.GpsToXZ(52.031157f, 5.332028f, mesh.PivotLatitude, mesh.PivotLongitue);
		//GameObject.Find("Game").transform.position = tmp;
		
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser) {
			GPSStatusText.text = "Location service is disabled";
			yield break;
		}
		
		// Start service before querying location
		Input.location.Start (1f, 1f);
		Input.compass.enabled = true;
		
		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);
			maxWait--;
		}
		
		serviceEnabled = true;
		for (int i = 0; i<numberOfIterations; i++){
			previous_orientations.Add(280);
		}
		
		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			print("Timed out");
			GPSStatusText.text = "Timed out";
			serviceEnabled = false;
			
			yield break;
		}
		
		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			GPSStatusText.text = "Unable to determine device location";
			print("Unable to determine device location");
			serviceEnabled = false;
			
			yield break;
		}
		else
		{
			lastLatitude = Input.location.lastData.latitude;
			lastLongitude = Input.location.lastData.longitude;
			gps.SetState(lastLatitude, lastLongitude, Input.location.lastData.horizontalAccuracy, (long)Input.location.lastData.timestamp);
			//transform.position = Tools.GpsToXZ(lastLatitude,lastLongitude, mesh.PivotLatitude, mesh.PivotLongitue);
			GPSStatusText.text = "Location service working";
			// Access granted and location value could be retrieved
			//text.text = "Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp;
			print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
		}
		
		if (!SystemInfo.supportsGyroscope) {
			GPSStatusText.text = "Gyroscope not supported";
			yield break;
		}
		
		// Stop service if there is no need to query location updates continuously
		//Input.location.Stop();
	}
	
	// Update is called once per frame
	void Update () {
		/*if (gyroBool) {
			quatMap = gyro.attitude;
			transform.localRotation = quatMap * quatMult;
		}*/
		if(Application.isEditor){
			HandleOrientation();
			HandleSounds();
		}
		//if service is enabled and one second is elapsed, update the scene
		if (serviceEnabled) {
			HandleOrientation();
			HandleSounds();
			float latitude = Input.location.lastData.latitude;
			float longitude = Input.location.lastData.longitude;
			
			Vector3 newPos = transform.position;
			gps.set_Q(moveMultSlider.value);
			
			//if(latitude == lastLatitude && longitude == lastLongitude){
				//integrate position by lineair velocity
				/*if(previousVelocities.Count == 4){
					var estimatedVelocity = Tools.RungeKutta4(
						previousVelocities[0], 
						previousVelocities[1], 
						previousVelocities[2], 
						previousVelocities[3]);
					newPos += estimatedVelocity;
				}*/
			//}else{
				//gps.Process(latitude, longitude, Input.location.lastData.horizontalAccuracy, (long)Input.location.lastData.timestamp);
				//newPos = Tools.GpsToXZ((float)gps.get_lat(), (float)gps.get_lng(), mesh.PivotLatitude, mesh.PivotLongitue);
				gpsMarker.transform.position = Tools.GpsToXZ(latitude, longitude, mesh.PivotLatitude, mesh.PivotLongitue);
				newPos = gpsMarker.transform.position;
				
				if(!mesh.IsOnMesh(newPos)){
					var meshPt = mesh.NearestPoint(mesh.transform.InverseTransformPoint(newPos));
					Vector3 worldPt = mesh.transform.TransformPoint(meshPt);
					transform.position = worldPt;
				}else{
					transform.position = newPos;
				}
			//}
			
			Velocity = (transform.position - LastPos) / Time.deltaTime;
			
			lastLatitude = latitude;
			lastLongitude = longitude;
			LastPos = transform.position;
			
			if(previousVelocities.Count > 3){
				previousVelocities.RemoveAt(0);
			}
			previousVelocities.Add(Velocity);
			
			var accuracy = Input.location.lastData.horizontalAccuracy;
			AccuracySphere.transform.localScale = new Vector3(accuracy*2, accuracy*2, accuracy*2);
			AccuracySphere.transform.position = gpsMarker.transform.position;
			
			PositionText.text = string.Format("Cam pos: {0},{1}", transform.position.x, transform.position.z);
			PositionText.text += '\n';
			PositionText.text += string.Format("Gps pos: {0},{1}", gpsMarker.transform.position.x, gpsMarker.transform.position.z);
			
			GPSStatusText.text = string.Format("Error: {0}", gps.get_accuracy().ToString());
			GPSStatusText.text += '\n';
			GPSStatusText.text += string.Format("GPS accuracy: {0}m", accuracy);
			GPSStatusText.text += '\n';
			GPSStatusText.text += string.Format("fps: {0}", Time.frameCount / Time.time);
		}
	}
	
	void HandleOrientation(){

		new_player_orientation = Application.isEditor ? 0f : Input.compass.trueHeading;
		previous_orientations.Add(new_player_orientation);
		previous_orientations.RemoveAt(0);

		float t1 = player_orientation;
		float average = 0;
		
		if(previous_orientations.Count == numberOfIterations){
			for(int i=0; i<numberOfIterations; i++){
				average += getDegrees(previous_orientations[i], t1);
			}
			average = average/numberOfIterations;
			player_orientation = t1 +average;
		}
		
		if (player_orientation < 0) player_orientation= player_orientation + 360;
		if (player_orientation > 360) player_orientation = player_orientation - 360;

		snitch_direction = Vector3.Angle(transform.forward, (transform.position - snitch.transform.position).normalized);
		Debug.DrawRay(transform.position, transform.forward * 100);

		directionAudio.volume = 1- fixDirection(snitch_direction)/180f;
		transform.eulerAngles = new Vector3 (transform.eulerAngles.x, player_orientation, transform.eulerAngles.z);
	}
	
	void HandleSounds(){
		if(!GetAudioPlayer(AudioPlayerType.Story).isPlaying && !GetAudioPlayer(AudioPlayerType.HeartRateFeedback).isPlaying){
			if(!directionAudio.isPlaying)
				directionAudio.Play();
			
			if(!snitch.GetComponent<Snitch>().IsInitialized())
				snitch.GetComponent<Snitch>().Resume();
		}else if(directionAudio.isPlaying || snitch.GetComponent<Snitch>().IsInitialized()){
			directionAudio.Stop();
			snitch.GetComponent<Snitch>().Pause();
		}
	}
	
	float fixDirection(float value){
		float d;
		if(value<180)
			d = value;
		else
			d = 360-value;

		return d;
	}

	float getDegrees(float previous_orientation, float player_orientation){
		float value = previous_orientation - player_orientation;
		if (value>180f){
			return value-360f;
		} else if (value <-180f){
			return value +360f;
		} else{
			return value;
		}
	}
	private AudioSource GetAudioPlayer(AudioPlayerType type){
		return GetComponents<AudioSource>()[(int)type];
	}
	public void Advance(){
		//play the story
		
		var lastClipNum = StoryClips.IndexOf(GetAudioPlayer(AudioPlayerType.Story).clip);
		PlayClip(StoryClips[lastClipNum+1]);
	}
	
	private void PlayClip(AudioClip clip){
		var playerAudioSource = GetAudioPlayer(AudioPlayerType.Story);
		playerAudioSource.clip = clip;
		playerAudioSource.Play();
	}
	public void PlayAudio(string name, bool loop = false){
		var audioClip = StoryClips.FirstOrDefault(sc => sc.name == name);
		
		if(audioClip != null){
			var playerAudioSource = GetAudioPlayer(AudioPlayerType.Story);
			playerAudioSource.clip = audioClip;
			playerAudioSource.loop = loop;
			playerAudioSource.Play();
		}
	}
	
	public enum AudioPlayerType{
		DirectionStatic = 0,
		Story = 1,
		HeartRateFeedback = 2
	}
	
	public void StopAudio(AudioPlayerType type){
		GetAudioPlayer(type).Stop();
	}
	
	public void StartHeartBeat(){
		if(!GetAudioPlayer(AudioPlayerType.HeartRateFeedback).isPlaying)
			GetAudioPlayer(AudioPlayerType.HeartRateFeedback).Play();
	}
	
	public void StopHeartBeat(){
		GetAudioPlayer(AudioPlayerType.HeartRateFeedback).Stop();
	}
	
	public void GiveFeedback(Game.FeedbackType type){
		var fbPlayer = GetAudioPlayer(AudioPlayerType.HeartRateFeedback);
		
		if(type == Game.FeedbackType.Faster){
			var randomClipIndex = Random.Range(0, FeedbackFaster.Count-1);
			fbPlayer.clip = FeedbackFaster[randomClipIndex];
		}else{
			var randomClipIndex = Random.Range(0, FeedbackSlower.Count-1);
			fbPlayer.clip = FeedbackSlower[randomClipIndex];
		}
		fbPlayer.Play();
	}
}
