﻿using UnityEngine;
using System.Collections;

public static class Tools {

	public static Vector3 PolarToCartesian(float latitude, float longitude, float radius)
	{
		//float earthRadius = 60000;
		double coordY = radius * Mathf.Cos(latitude) * Mathf.Sin(longitude); 
		double coordX = radius * Mathf.Cos(latitude) * Mathf.Cos(longitude); 
		//double coordZ = earthRadius*Mathf.Sin(latitude);
		
		Vector3 Cartesian; 
		Cartesian.x = (float)coordY;//(float)coordX; 
		Cartesian.y = (float)0; 
		Cartesian.z = (float)coordX;//(float)coordZ;
		
		return Cartesian;
	}

	public static Vector3 GpsToXZ(float sourceLat, float sourceLon, float originLat, float originLon){
		//origin = {slat:slat, slon:slon,coord_system:1,olat:olat,olon:olon,xoffset_mtrs:0,yoffset_mtrs:0,rotation_angle_degs:0,rms_error:0};

		float xx,yy,r,ct,st,angle;
		angle = Mathf.Deg2Rad * 0;//*rotation_angle_degs
		
		xx = (sourceLon - originLon) * MetersDegLon(originLat);
		yy = (sourceLat - originLat) * MetersDegLat(originLat);
		
		r = Mathf.Sqrt(xx * xx + yy * yy);

		if(r != 0)
		{
			ct = xx/r;
			st = yy/r;
			xx = r * ( (ct * Mathf.Cos(angle)) + (st * Mathf.Sin(angle)) );
			yy = r * ( (st * Mathf.Cos(angle)) - (ct * Mathf.Sin(angle)) );
		}

		float Xcord = xx + 0;//+xoffset_mtrs;
		float Ycord = yy + 0;//+yoffset_mtrs;

		return new Vector3 (Xcord, 0, Ycord);
	}

	private static float MetersDegLon(float x)
	{  
		var d2r = Mathf.Deg2Rad * x;

		return((111415.13f * Mathf.Cos(d2r))- (94.55f * Mathf.Cos(3.0f*d2r)) + (0.12f * Mathf.Cos(5.0f*d2r)));
	}

	private static float MetersDegLat(float x)
	{
		var d2r = Mathf.Deg2Rad * x;

		return(111132.09f - (566.05f * Mathf.Cos (2.0f * d2r)) + (1.20f * Mathf.Cos (4.0f * d2r)) - (0.002f * Mathf.Cos (6.0f * d2r)));
	}

	public static float Distance(float sourceLat, float sourceLon, float destLat, float destLon){
		/* a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
		 * c = 2 ⋅ atan2( √a, √(1−a) )
		 * d = R ⋅ c
		 * where 	φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);
		 */

		var R = 6371000; // metres
		var φ1 = sourceLat;//lat1.toRadians();
		var φ2 = destLat;//lat2.toRadians();
		var Δφ = (destLat-sourceLat);//.toRadians();
		var Δλ = (destLon - sourceLon);//.toRadians();
		
		var a = Mathf.Sin(Δφ/2) * Mathf.Sin(Δφ/2) +	Mathf.Cos(φ1) * Mathf.Cos(φ2) *	Mathf.Sin(Δλ/2) * Mathf.Sin(Δλ/2);
		var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1-a));
		
		var d = R * c;

		return d;
	}
	
	public static Vector3 RungeKutta4(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4){
		/* v(t+dt) = v(t) + (v1 + 2⋅v2 + 2⋅v3 + v4)/6
		 * where v(t) is v4, v1 is velocity 4 timesteps ago, v2 is 3 timesteps ago, etc
		 */  
		 
		 var newV = v4 + (v1+2*v2+2*v3+v4)/6;
		 return newV;
	}
	
	public static Vector3 NearestPointOnMesh(Vector3 pt, Vector3[] verts, KDTree vertProx, int[] tri, VertTriList vt) {
	//	First, find the nearest vertex (the nearest point must be on one of the triangles
	//	that uses this vertex if the mesh is convex).
		int nearest = vertProx.FindNearest(pt);
		
	//	Get the list of triangles in which the nearest vert "participates".
		int[] nearTris = vt[nearest];
		
		Vector3 nearestPt = Vector3.zero;
		float nearestSqDist = 100000000f;
		
		for (int i = 0; i < nearTris.Length; i++) {
			int triOff = nearTris[i] * 3;
			Vector3 a = verts[tri[triOff]];
			Vector3 b = verts[tri[triOff + 1]];
			Vector3 c = verts[tri[triOff + 2]];
			
			Vector3 possNearestPt = NearestPointOnTri(pt, a, b, c);
			float possNearestSqDist = (pt - possNearestPt).sqrMagnitude;
			
			if (possNearestSqDist < nearestSqDist) {
				nearestPt = possNearestPt;
				nearestSqDist = possNearestSqDist;
			}
		}
		
		return nearestPt;
	}
	
	
	public static Vector3 NearestPointOnMesh(Vector3 pt, Vector3[] verts, int[] tri, VertTriList vt) {
	//	First, find the nearest vertex (the nearest point must be on one of the triangles
	//	that uses this vertex if the mesh is convex).
		int nearest = -1;
		float nearestSqDist = 100000000f;
		
		for (int i = 0; i < verts.Length; i++) {
			float sqDist = (verts[i] - pt).sqrMagnitude;
			
			if (sqDist < nearestSqDist) {
				nearest = i;
				nearestSqDist = sqDist;
			}
		}
		
	//	Get the list of triangles in which the nearest vert "participates".
		int[] nearTris = vt[nearest];
		
		Vector3 nearestPt = Vector3.zero;
		nearestSqDist = 100000000f;
		
		for (int i = 0; i < nearTris.Length; i++) {
			int triOff = nearTris[i] * 3;
			Vector3 a = verts[tri[triOff]];
			Vector3 b = verts[tri[triOff + 1]];
			Vector3 c = verts[tri[triOff + 2]];
			
			Vector3 possNearestPt = NearestPointOnTri(pt, a, b, c);
			float possNearestSqDist = (pt - possNearestPt).sqrMagnitude;
			
			if (possNearestSqDist < nearestSqDist) {
				nearestPt = possNearestPt;
				nearestSqDist = possNearestSqDist;
			}
		}
		
		return nearestPt;
	}
	
	
	public static Vector3 NearestPointOnTri(Vector3 pt, Vector3 a, Vector3 b, Vector3 c) {
		Vector3 edge1 = b - a;
		Vector3 edge2 = c - a;
		Vector3 edge3 = c - b;
		float edge1Len = edge1.magnitude;
		float edge2Len = edge2.magnitude;
		float edge3Len = edge3.magnitude;
		
		Vector3 ptLineA = pt - a;
		Vector3 ptLineB = pt - b;
		Vector3 ptLineC = pt - c;
		Vector3 xAxis = edge1 / edge1Len;
		Vector3 zAxis = Vector3.Cross(edge1, edge2).normalized;
		Vector3 yAxis = Vector3.Cross(zAxis, xAxis);
		
		Vector3 edge1Cross = Vector3.Cross(edge1, ptLineA);
		Vector3 edge2Cross = Vector3.Cross(edge2, -ptLineC);
		Vector3 edge3Cross = Vector3.Cross(edge3, ptLineB);
		bool edge1On = Vector3.Dot(edge1Cross, zAxis) > 0f;
		bool edge2On = Vector3.Dot(edge2Cross, zAxis) > 0f;
		bool edge3On = Vector3.Dot(edge3Cross, zAxis) > 0f;
		
	//	If the point is inside the triangle then return its coordinate.
		if (edge1On && edge2On && edge3On) {
			float xExtent = Vector3.Dot(ptLineA, xAxis);
			float yExtent = Vector3.Dot(ptLineA, yAxis);
			return a + xAxis * xExtent + yAxis * yExtent;
		}
		
	//	Otherwise, the nearest point is somewhere along one of the edges.
		Vector3 edge1Norm = xAxis;
		Vector3 edge2Norm = edge2.normalized;
		Vector3 edge3Norm = edge3.normalized;
		
		float edge1Ext = Mathf.Clamp(Vector3.Dot(edge1Norm, ptLineA), 0f, edge1Len);
		float edge2Ext = Mathf.Clamp(Vector3.Dot(edge2Norm, ptLineA), 0f, edge2Len);
		float edge3Ext = Mathf.Clamp(Vector3.Dot(edge3Norm, ptLineB), 0f, edge3Len);

		Vector3 edge1Pt = a + edge1Ext * edge1Norm;
		Vector3 edge2Pt = a + edge2Ext * edge2Norm;
		Vector3 edge3Pt = b + edge3Ext * edge3Norm;
		
		float sqDist1 = (pt - edge1Pt).sqrMagnitude;
		float sqDist2 = (pt - edge2Pt).sqrMagnitude;
		float sqDist3 = (pt - edge3Pt).sqrMagnitude;
		
		if (sqDist1 < sqDist2) {
			if (sqDist1 < sqDist3) {
				return edge1Pt;
			} else {
				return edge3Pt;
			}
		} else if (sqDist2 < sqDist3) {
			return edge2Pt;
		} else {
			return edge3Pt;
		}
	}
}
