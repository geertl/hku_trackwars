﻿using UnityEngine;
using System.Collections;

public class OverviewCameraMover : MonoBehaviour {

	public float orthoZoomSpeed = 1f;
	public float orthoScrollSpeed = 0.5f;
	
	// Update is called once per frame
	void Update () {
		int nbTouches = Input.touchCount;

		if(nbTouches == 1)
		{
			Touch touch = Input.GetTouch(0);
	
			TouchPhase phase = touch.phase;
			
			switch(phase)
			{
				case TouchPhase.Began:
					
					break;
				case TouchPhase.Moved:
					
					transform.position -= new Vector3(touch.deltaPosition.x, 0, touch.deltaPosition.y) * orthoScrollSpeed;
					break;
				case TouchPhase.Ended:
					
					break;
			}
		}
		else if(nbTouches == 2){
			Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            var camera = GetComponent<Camera>();
			// ... change the orthographic size based on the change in distance between the touches.
			camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

			// Make sure the orthographic size never drops below zero.
			camera.orthographicSize = Mathf.Max(camera.orthographicSize, 0.1f);
		}
	}
}
