﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class Game : MonoBehaviour {
	//MeshProperties mesh;
	private AndroidJavaClass unity;
	private AndroidJavaObject currentActivity;
	private Text StatusText;
	private Text HeartRateText;

	public List<GameObject> meshes;
	public MeshProperties CurrentMesh;
	public Snitch snitch;
	public Player player;
	public string loc = "MRC";
	public string path = "1.1";

	public int minHeartRate = 105;
	public int maxHeartRate = 130;
	
	public int _debug_heartrate = 100;
	public float notLevelRespawnTime = 30f;
	public float notLevelFeedbackTime = 5f;
	public float notLevelFeedbackDelay = 10f;
	public float notLevelRespawnRadius = 100f;
	public float notLevelRespawnMultiplier = 10.0f;
	private float notLevelTime = 0.0f;
	private float notLevelTimeFeedback = 0.0f;

	public int HeartRate { 
		get {
			if(Application.isEditor)
				return _debug_heartrate;
				
			if(currentActivity == null)
				return 0;
							
			int hr = 0;
			var success = int.TryParse(currentActivity.Get<string>("HeartRateText"), out hr);
			return (int)Mathf.Abs(hr);
		}
	}

	void Start(){
		StatusText = GameObject.Find ("BTStatusText").GetComponent<Text> ();
		HeartRateText = GameObject.Find ("HeartRateText").GetComponent<Text> ();
		
		if(Application.platform == RuntimePlatform.Android){
			unity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		}else{
			StatusText.text = "Not running on Android; bluetooth not working";
		}	
	}

	public void ToggleCameraView(){
		var overviewCam = GameObject.Find("Overview Camera").GetComponent<Camera>();
		var playerCam = player.gameObject.GetComponent<Camera>();
		
		overviewCam.enabled = !overviewCam.enabled;
		playerCam.enabled = !playerCam.enabled;

	}
	
	public void ConnectBT(){
		if(Application.platform != RuntimePlatform.Android)
			return;
		
		currentActivity = unity.GetStatic<AndroidJavaObject> ("currentActivity");
		var btnText = GameObject.Find("ConnectButton").GetComponentInChildren<Text>();
		
		if(currentActivity.Call<bool>("IsConnected")){
			Debug.Log("Disconnecting from bluetooth");
			currentActivity.Call("Disconnect");
			btnText.text = "Connect";
		}else{
			Debug.Log("Connecting to bluetooth");
			currentActivity.Call("Connect");
			btnText.text = "Disconnect";			
		}
	}

	public void toggleMesh(){
		var mesh = meshes.FirstOrDefault(m => m.name != loc);
		player.mesh = mesh.GetComponent<MeshProperties>();
		loc = mesh.name;
		CurrentMesh = player.mesh;
		
		foreach(var m in meshes)
			m.gameObject.SetActive(m.name == mesh.name);
		
	}

	void Update(){
		if(HeartRateText != null && StatusText != null && currentActivity != null){
			HeartRateText.text = "HeartRate: " + HeartRate.ToString();
			var hrp = GetHeartRatePenalty();
			HeartRateText.text += '\n' + "Penalty: " + hrp.ToString();
			
			if(hrp > 0)
				HeartRateText.color = Color.red;
			else
				HeartRateText.color = new Color(50f, 50f, 50f);
				
			StatusText.text = currentActivity.Get<string>("tv");
		}
		
		//check if catching the snitch
		if(IsRunning()){
			if(snitch.IsWithinDistance()){
				//snitch caught
				if(notLevelTime / notLevelRespawnTime > 1){
					snitch.SpawnRandomly(notLevelRespawnRadius + (notLevelTime / notLevelRespawnTime) * notLevelRespawnMultiplier);
					notLevelTime = 0.0f;
				}else{
					StoryAdvance();
				}
			}
			
			if(!IsLevel()){
				notLevelTime += Time.deltaTime;
				notLevelTimeFeedback += Time.deltaTime;
				//not level (waterpas), give feedback
				//player.StartHeartBeat();
				if(notLevelTimeFeedback / notLevelFeedbackTime > 1){
					var penalty = GetHeartRatePenalty();
					player.GiveFeedback(penalty > 0 ? FeedbackType.Slower : FeedbackType.Faster);
					notLevelTimeFeedback = -notLevelFeedbackDelay;
				}
			}else{
				//level (waterpas), stop heartbeat
				//player.StopHeartBeat();
			}
		}
	}
	public enum FeedbackType{
		Slower,
		Faster
	}
	
	public void StoryAdvance(){
		snitch.Advance();
		player.Advance();
	}
	
	private bool IsRunning(){
		return (currentActivity != null || Application.isEditor) && snitch.IsInitialized();
	}
	
	public int GetHeartRatePenalty(){
		var heartRate = HeartRate;
		//var diffHr = heartRate < minHeartRate ? minHeartRate - heartRate : (heartRate > maxHeartRate ? heartRate - maxHeartRate : 0);
		var diffHr = heartRate < minHeartRate ? heartRate - minHeartRate : (heartRate > maxHeartRate ? heartRate - maxHeartRate : 0);
	
		return diffHr;
	}

	public bool IsLevel(){
		return GetHeartRatePenalty() == 0;
	}
}
