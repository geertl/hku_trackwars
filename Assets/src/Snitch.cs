﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class Snitch : MonoBehaviour {
	
	private GameObject camera;
	private AudioSource beep;
	private AudioSource approaching;
	private float pathPosition = 0f;
	public float snitchSpeed = 0.05f;
	public float beepSpeed = 1f;
	public Vector3[] nodes;
	public float maxHearingDistance = 150f;
	public float maxSnitchDistance = 30f;
	public float heartRateCoef = 2.0f;
	public float minSnitchCatchDistance = 5f;
	public float minSnitchApproachingDistance = 50f;
	
	private bool withinSnitchDistance = false;
	private Game game;
	private Text heartRateText;
	private bool initialized = false;
	private int currentPathNum = 0;
	private bool isRandomLocation = false;

	// Use this for initialization
	void Start(){
		camera = GameObject.FindGameObjectWithTag ("MainCamera");
		beep = GetComponents<AudioSource> ()[0];
		approaching = GetComponents<AudioSource> ()[1];
		game = GameObject.Find("Game").GetComponent<Game>();
		
		Initialize(1);
		Pause();
	}
	
	public void Pause(){
		initialized = false;
		approaching.Stop();
	}
	public void Resume(){
		initialized = true;
		approaching.Play();		
	}
	
	public bool IsInitialized() {return initialized;}
	
	void Initialize(int pathNum){
		var path = GameObject.Find("1."+pathNum.ToString());
		
		if(path == null){
			path = GameObject.Find("1.0");
			currentPathNum = 0;
		}else{
			currentPathNum = pathNum;
		}
		
		nodes = path.GetComponent<iTweenPath>().nodes.ToArray();
		transform.position = iTween.PointOnPath(nodes, pathPosition%1);
		
		initialized = true;
		withinSnitchDistance = false;
		isRandomLocation = false;
	}

	void Update () {
		if(!initialized) return;

		//calculate the distance to the player
		var diff = transform.position - camera.transform.position;
		var mag = diff.magnitude;

		//play beep sound accounting for distance from player
		//if (!beep.isPlaying && mag < maxHearingDistance && mag > 0f)
		//	beep.PlayDelayed ((mag / maxHearingDistance) / beepSpeed);
		if(!approaching.isPlaying && mag < minSnitchApproachingDistance)
			approaching.Play();
		if(approaching.isPlaying && mag > minSnitchApproachingDistance)
			approaching.Stop();
		
		//set bool if within the min snitch catch distance
		withinSnitchDistance = mag <= minSnitchCatchDistance && mag > 0;
		
		//var hrPenalty = ((float)game.GetHeartRatePenalty() / 140) * heartRateCoef;
		if(!isRandomLocation){
			if(mag <= maxSnitchDistance && mag > 0) {
				pathPosition += ((maxSnitchDistance)/ mag) * snitchSpeed * Time.deltaTime;
			}
	
			float pathPercent = pathPosition %1;
			Vector3 coordinateOnPath = iTween.PointOnPath(nodes, pathPercent);
			transform.position = coordinateOnPath;
		}
	}

	public bool IsWithinDistance(){
		return withinSnitchDistance;
	}

	public void Advance(){
		//if(!initialized){
		//	Initialize(currentPathNum);
		//}else{
			Initialize(currentPathNum+1);
		//}
	}
	
	public void SpawnRandomly(float radius){
		isRandomLocation = true;
		
		var angle = UnityEngine.Random.value * Mathf.PI * 2;
		//var radius = 10;//Mathf.Sqrt(Random.value) * 10;
		var x = camera.transform.position.x + radius * Mathf.Cos(angle);
		var z = camera.transform.position.z + radius * Mathf.Sin(angle);

		var pos = new Vector3((int)x, 0f, (int)z);
		if(!game.CurrentMesh.IsOnMesh(pos)){
			var meshPt = game.CurrentMesh.NearestPoint(game.CurrentMesh.transform.InverseTransformPoint(pos));
			Vector3 worldPt = game.CurrentMesh.transform.TransformPoint(meshPt);
			
			transform.position = worldPt;
		}else{
			transform.position = pos;
		}
	}
	
}
