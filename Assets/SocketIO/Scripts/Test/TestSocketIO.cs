#region License
/*
 * TestSocketIO.cs
 *
 * The MIT License
 *
 * Copyright (c) 2014 Fabio Panettieri
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#endregion

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;
//using Owl.Converter;
using System;

public class myObj{
	public string id{get; set;}
}

public class TestSocketIO : MonoBehaviour
{
	private SocketIOComponent socket;
	int timer = 0;

	public int device_id =0;

	private double lat;
	private double p_lat;
	private double lon;
	private double p_lon;

	private int heartRate;

	private List<int> heartRates = new List<int>();

	private int p_heartRate;
	private Vector2 clientCoords;
	private Vector2 lastCoords;

	private Text userID;
	//private string user = "";
	public bool userIsConnected = false;
	private bool sendData = false;
    private Guid UnityId;
    private int? TrainerId;
    private List<int> IndexedTrainerIds;
    private Dropdown gameSelect;

    public void Start() 
	{
        UnityId = Guid.NewGuid();
        IndexedTrainerIds = new List<int>();
        gameSelect = GameObject.Find("GameSelect").GetComponent<Dropdown>();
        gameSelect.options.Clear();

        userID = GameObject.Find("userID").GetComponent<Text>() as Text;

		DontDestroyOnLoad(transform.gameObject);


		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		socket.On("open", TestOpen);
		socket.On("boop", TestBoop);
		socket.On("receiveID", receiveID); 
		socket.On("waterpas", updateWaterpas);
		socket.On ("startGame", startGame);
		socket.On("error", TestError);
		socket.On("close", TestClose);
        socket.On("receiveActiveGames", ReceiveActiveGames);
        
	}

    void Update(){
		if (userIsConnected == false){
            if(TrainerId != null)
                requestID();

            getActiveGames();
		}
		else{

			if (sendData == true && device_id !=0){
				try{
					var playerCam = GameObject.Find("Player");
					var game = GameObject.Find("Game");
					
					if(playerCam != null && game != null){
						heartRates.Add(game.GetComponent<Game>().HeartRate);
						lat = playerCam.GetComponent<Player>().lastLatitude;
						lon = playerCam.GetComponent<Player>().lastLongitude;
						
						clientCoords = new Vector2(playerCam.transform.position.x, playerCam.transform.position.z);
					}
				} catch (Exception e){
					Debug.LogException (e, this);
				}
				
				if (!(clientCoords.x == lastCoords.x && clientCoords.y == lastCoords.y)){
					// put dataTransfer(); here to only send data when GPS is changed
					lastCoords = clientCoords;

					heartRate = (int)heartRates.Average();

					dataTransfer();

					heartRates.Clear();
				}
			}
		}
	}



	public void requestID(){
		timer++;
		if (timer>40){
            var data = new Dictionary<string, string>();
            data["unityId"] = UnityId.ToString();
            data["trainerId"] = TrainerId.ToString();

			socket.Emit("requestID", new JSONObject(data));
			timer=0;
		}
	}

    public void getActiveGames()
    {
        timer++;
        if (timer > 40)
        {
            var data = new Dictionary<string, string>();
            data["unityId"] = UnityId.ToString();
            var obj = new JSONObject(data);
            
            socket.Emit("getActiveGames", obj);
            timer = 0;
        }
    }

    public void updateWaterpas(SocketIOEvent e){

		int minHR = int.Parse(e.data["minimum_HR"].ToString());
		int maxHR = int.Parse(e.data["maximum_HR"].ToString());

		GameObject.Find("Game").GetComponent<Game>().minHeartRate = minHR;
		GameObject.Find("Game").GetComponent<Game>().maxHeartRate = maxHR;
	}

	public void startGame(SocketIOEvent e){
		if(Application.loadedLevelName != "Main"){
			Application.LoadLevel("main");
			Debug.Log("Start");
			sendData = true;
		}
	}


	public void receiveID(SocketIOEvent e){
		var unityId = e.data["unityId"].str;

        if (unityId != UnityId.ToString())
            return;
			
		if (userIsConnected ==  false){
			device_id = int.Parse(e.data["device_id"].ToString());

			var user_id = e.data["user_id"];
			Debug.Log(e.data["device_id"]);
			Debug.Log("device id: " + device_id + " - user id: " + user_id);
		
			userID.text = device_id.ToString();
			userIsConnected = true;
		}

	}


	public void dataTransfer(){
		Dictionary<string, string> data = new Dictionary<string, string>();
		//data["clientId"] = 1; // id van de server teruggekregen

		data["device_id"] = device_id.ToString();
		data["lat"] = lat.ToString("0.000000");
		data["lon"] = lon.ToString("0.000000");
		data["heartrate"] = heartRate.ToString();
		data["x"] = clientCoords.x.ToString();
		data["y"] = clientCoords.y.ToString();

		socket.Emit("dataTransfer", new JSONObject(data));

		p_lat = lat;
		p_lon = lon;
	}

    public void OnGameSelected()
    {
        TrainerId = IndexedTrainerIds[gameSelect.value];
    }

    public void ReceiveActiveGames(SocketIOEvent e)
    {
        var unityId = e.data["unityId"].str;

        if (unityId != UnityId.ToString())
            return;

        gameSelect.options.Clear();
        IndexedTrainerIds.Clear();

        var games = e.data["games"].list;
        if (games.Count == 1)
        {
            TrainerId = (int)games[0]["trainer"]["id"].n;
            gameSelect.gameObject.SetActive(false);
        }
        else
        {
            gameSelect.gameObject.SetActive(true);

            foreach (var game in games)
            {
                var trainer = game["trainer"];

                IndexedTrainerIds.Add((int)trainer["id"].n);
                gameSelect.options.Add(new Dropdown.OptionData(string.Format("{0} {1}", trainer["firstName"].str, trainer["lastName"].str)));
            }

            if (TrainerId != null)
                gameSelect.value = (int)TrainerId;
        }
    }

    public void TestOpen(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Open received: " + e.name + " " + e.data);
	}
	
	public void TestBoop(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Boop received: " + e.name + " " + e.data);
		
		if (e.data == null) { return; }
		
		Debug.Log(
			"#####################################################" +
			"THIS: " + e.data.GetField("this").str +
			"#####################################################"
			);
	}
	
	public void TestError(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Error received: " + e.name + " " + e.data);
	}
	
	public void TestClose(SocketIOEvent e)
	{	
		Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);
	}


}
